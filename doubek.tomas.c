#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "random.h"
#include "isle.h"

/** Tomas Doubek 74656
 *
 * 
 * Semestralne zadanie 2014
 * last update:  11.12.2014
 *
 */

// =============================================== NASTAV STRUKTURU ======================================
/** zakladna inicializacia ostrovov - odvolava sa na meno struktury _isle_t v isle.h */
struct _isle_t * ostrovy[100];

// =============================================== NASTAV POCET ROKOV ======================================

/**< funkcia nastavi pocet rokov (cast ulohy s GUI) */
int set_pocet_rokov()
{
    int rokov;
    printf ("Zadajte pocet rokov.");
    scanf("%d", &rokov);
    return rokov;
}
// =============================================== NASTAV POCET OSTROVOV ======================================

/**< funkcia nastavi pocet ostrovov (cast ulohy s GUI) */
int set_pocet_ostrovov()
{
    int ostrovov;
    printf ("Zadajte pocet ostrovov, s ktoryme chcete hrat.");
    scanf("%d", &ostrovov);
    return ostrovov;
}

// =============================================== CIARA ======================================
/**< vseobecne volanie ciary cez "=" na 80 znaov v riadku */
void ciara()
{
    int i;
    printf("\n");
    for(i = 0; i<80; i++)
    {
        printf("=");
    }
    printf("\n");
}

// =============================================== INTRO ======================================
/**< zobrazenie uvodneho POKECU k hre, cast GUI */
void intro()
{
    printf("Vitajte na planete DESIGNDNT\n.");
    printf("Prisli ste z dalekeho vesmira a vasou ulohou bude vybudovat co najprospereujucejsie ostrovy na planete.\n");
    printf("Prosperujuci ostrov je ostrov, ktory bude mat najviac vlajok MOAI\n");
    printf("Vlajku MOAI postavite len vtedy, ak budete mat viac ako:\n");
    printf("50 obyvatelov\n");
    printf("80 dreva\n");
    printf("3 dediny\n");
    ciara();
}
// =============================================== POROVNAJ ZNAK - PRI SAVE ======================================
/**< funkcia porovna znak, ak sa znak zhoduje, vrati true, inak false. Neskor vo funkciach porovnaj_znak_load/porovnaj_znak_save sa na zaklade logickej hodnoty riesi uloha nacitania/nenacitania zo s�boru; ulozenia/neulozenia do suboru  */
int porovnaj_znak()
{
    char znak;
    int result;
    printf("CHCETE ULOZT HRU?\n\n");
    printf("Stlacte Y pre ulozenie aktualneho stavu hry do NOVEHO SUBORU pod custom menom \n");
    printf("Stlacte N pre ulozenie aktualneho stavu hry do DEFAULT SUBORU \n");
    printf("Stlacte LUBOVOLNU KLAVESU pre pokracovanie v hre bez ulozenia\n");
    printf("vasa akcia: ");
    scanf(" %c", &znak);

    if (znak == 'Y' || znak == 'y')
    {
        result = 1;
    }
    else if (znak == 'N' || znak == 'n')
    {
        result = 2;
    }
    else
    {
        result = 0;
    }
    return result;

}

// =============================================== POROVNAJ ZNAK - PRI LOADE ======================================
/**< funkcia porovna znak, ak sa znak zhoduje, vrati true, inak false. Neskor vo funkciach porovnaj_znak_load/porovnaj_znak_save sa na zaklade logickej hodnoty riesi uloha nacitania/nenacitania zo s�boru; ulozenia/neulozenia do suboru  */
int porovnaj_znak_load()
{
    char znak;
    int result;
    printf("CHCETE NACITAT ULOZENU HRU ZO SUBORU?\n\n");
    printf("Stlacte L pre nacitanie CUSTOM suboru\n");
    printf("Stlacte D pre nacitanie DEFAULT suboru\n");
    printf("Stlacte N pre novu hru\n");
    printf("vasa akcia: ");
    scanf(" %c", &znak);

    if (znak == 'L' || znak == 'l')
    {
        result = 1;
    }
    else if (znak == 'D' || znak == 'd')
    {
        result = 2;
    }
    else
    {
        result = 0;
    }
    return result;
}

// =============================================== FUNKCIA GUI VRATI CISLO AKCIE ======================================
/**< funkcia vracia cislo akcie, ktoru si pouzivatel zvoli */
int set_action()
{
    int action;
    printf("Stlacte 0 pre Budovanie farmy\n");
    printf("Stlacte 1 pre Budovanie dediny\n");
    printf("Stlacte 2 pre R�banie lesov\n");
    printf("Stlacte 3 pre Zbieranie dreva\n");
    printf("Stlacte 4 pre Zbieranie jedla\n");
    printf("Stlacte 5 pre Rybarenie\n");
    printf("Stlacte 6 pre Budovanie MOAI - vlajky\n");
    printf("Stlacte 7 pre Ziadna akcia\n");
    printf("vasa akcia: ");

    return scanf ("%d", &action);
}

// =============================================== FUNKCIA UMELEJ INTELIGENCIE ======================================

/** funkcia UMELEJ INTELIGENCIE
 *
 * 8.12.2014
 * Tomas Doubek
 * last update 10.12.2014
 * Funkcia vracia integer, jednu z akci.
 * Funkcia by mala pruzne reagovat na nahodne vygenerovany ostrov a pomocou algoritmov a spravneho tozhodovania sa buduje
 lepsia ekonomika. Ak je ekonomika na ostrove optimalna, postavi sa vlajka/socha MOAI.
 *
 */

int get_uniform_ui(Isle * p_isle[], int pocet_ostrovov)
{
    int akcia;
    int i;
    for(i = 0; i<=pocet_ostrovov; i++)
    {

        if((p_isle[i]->pop > GS(PopPerVillage)*GS(MoaiMultiplier)) && (p_isle[i]->wood > GS(WoodPerVillage)*GS(MoaiMultiplier)))
        {
            akcia = 6; //moai je najvyssia priorita
        }
        else if(p_isle[i]->wood < 1+(2*i))
        {
            akcia = 2; //chop forest
        }
        else if(p_isle[i]->food < 1+(2*i))
        {
            if (p_isle[i]->food >= p_isle[i]->pop)
            {
                akcia = 7;
            }
            else
            {
                akcia = 5;
            }
        }
        else if(p_isle[i]->pop < 1+(2*i))
        {
            akcia = rand() % 1 + 2; ;
        }


        else if(p_isle[i]->wood > GS(WoodPerFarm))
        {
            akcia = 1;
        }
        else if(p_isle[i]->food > 50)
        {
            akcia = rand() % 1 + 2; ;
        }
        else if ((p_isle[i]->pop > GS(PopPerVillage)*GS(MoaiMultiplier)) && (p_isle[i]->wood > GS(WoodPerVillage)*GS(MoaiMultiplier)) )
        {
            akcia = 2;
        }
        else
        {
            akcia = rand() % 1 + 5;
        }
        return akcia;
    }
}

// =============================================== MAX HODNOTY NA OSTROVE ======================================

/**< Funkcia vypise na ktorom ostrove je najviac danej suroviny. Vypise pocet surov�n a nazov ostrova, kde sa nachadza */
void max_na_ostrove(Isle * p_isle[], int pocet_ostrovov)
{
    ciara();

    printf("STAV NAJLEPSICH HODNOT NA OSTROVOVOCH PO TOMTO ROKU");
    printf("\n\n");
    int i;


    int max_forests = 0; //max sa na zaciatku nastavi na 0
    char *forests_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->forests > max_forests)
        {
            max_forests = p_isle[i]->forests; //zisti maximalny pocet lesov
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->forests == max_forests) // sa ten "maximalny pocet" rovna tomu nasmu maximalenu poxtu surovin pre LES, tak vypise meno ostorva.
        {
            forests_isle_name = p_isle[i]->name; // tu zisti meno ostrova
        }
    }
    printf("FOR: (nazov ostrova): %s \t (pocet): %d \n", forests_isle_name, max_forests); //vypise na obrazovku

    /** \brief
     *
     * opakuje sa pre vsetky suroviny :)
     *
     */

    int max_villages = 0;
    char *villages_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->villages > max_villages)
        {
            max_villages = p_isle[i]->villages;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->villages == max_villages)
        {
            villages_isle_name = p_isle[i]->name;
        }
    }
    printf("VIL: (nazov ostrova): %s \t (pocet): %d \n", villages_isle_name, max_villages);


    int max_farms = 0;
    char *farm_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->farms > max_farms)
        {
            max_farms = p_isle[i]->farms;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->farms == max_farms)
        {
            farm_isle_name = p_isle[i]->name;
        }
    }
    printf("FARM: (nazov ostrova): %s \t (pocet): %d \n", farm_isle_name, max_farms);

    int max_pop = 0;
    char *pop_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->pop > max_pop)
        {
            max_pop = p_isle[i]->pop;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->pop == max_pop)
        {
            pop_isle_name = p_isle[i]->name;
        }
    }
    printf("POP: (nazov ostrova): %s \t (pocet): %d \n", pop_isle_name, max_pop);


    int max_rats = 0;
    char *rats_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->rats > max_rats)
        {
            max_rats = p_isle[i]->rats;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->rats == max_rats)
        {
            rats_isle_name = p_isle[i]->name;
        }
    }
    printf("RAST: (nazov ostrova): %s \t (pocet): %d \n", rats_isle_name, max_rats);

    int max_food = 0;
    char *food_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->food > max_food)
        {
            max_food = p_isle[i]->food;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->food == max_food)
        {
            food_isle_name = p_isle[i]->name;
        }
    }
    printf("FOOD: (nazov ostrova): %s \t (pocet): %d \n", food_isle_name, max_food);


    int max_wood = 0;
    char *wood_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->wood > max_wood)
        {
            max_wood = p_isle[i]->wood;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->wood == max_wood)
        {
            wood_isle_name = p_isle[i]->name;
        }
    }
    printf("WOOD: (nazov ostrova): %s \t (pocet): %d \n", wood_isle_name, max_wood);

    int max_moai = 0;
    char *moai_isle_name;
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->moai > max_moai)
        {
            max_moai = p_isle[i]->moai;
        }
    }
    for(i = 0; i< pocet_ostrovov; i++)
    {
        if (p_isle[i]->moai == max_moai)
        {
            moai_isle_name = p_isle[i]->name;
        }
    }
    printf("WOOD: (nazov ostrova): %s \t (pocet): %d \n", moai_isle_name, max_moai);
    ciara();
}


// =============================================== NACITAJ INTEGER ======================================

/* funkcia nacita integer yo suboru */

int nacitaj_integer(const char* subor)
{
    FILE* file = fopen (subor, "r");
    int i = 0;

    fscanf (file, "%d", &i);
    fclose (file);
    return i;
}

// =============================================== READ ITEMS ======================================

/** \brief
 *
 * Povodny nazov funkcie: getline(), ktory som prevzal so stranky stuckowerflow.com. Funkciu som si modifikoval na readItem
 ktora sa pouziva vo fore a nacitavaju sa cez nu jednotlive ostrovy a polozky ostrovu su odelene bodkociarkou. Islo o to, ze
 som chcel ukladat data vo formate CSV pre pr�pad pouzivania v SQL DB alebo Matlabe podobne
 Jeden riadok = jeden ostrov.
 *
 */

char *readItem(FILE *file)
{

    if (file == NULL)
    {
        printf("Error: file pointer is null.");
        system("pause");
        exit(1);
    }

    int maximumLineLength = 128;
    char *lineBuffer = (char *)malloc(sizeof(char) * maximumLineLength);

    if (lineBuffer == NULL)
    {
        printf("Error allocating memory for line buffer.");
        exit(1);
    }

    char ch = getc(file);
    //printf("%c", ch);
    //char second;
    int count = 0;

    while ((ch != ';') && (ch != '\n') && (ch != EOF))
    {
        if (count == maximumLineLength)
        {
            maximumLineLength += 128;
            lineBuffer = realloc(lineBuffer, maximumLineLength);
            if (lineBuffer == NULL)
            {
                printf("Error reallocating space for line buffer.");
                exit(1);
            }
        }
        lineBuffer[count] = ch;
        count++;

        ch = getc(file);
        //printf("%c", ch);
        //second = getc(file);
    }
    //second = getc(file);

    lineBuffer[count] = '\0';
    return lineBuffer;
    /**< Tato cast fungovala len v compiolatore visual studio */
//    char line[count + 1];
//    strncpy(line, lineBuffer, (count + 1));

//   free(lineBuffer);
//    const char *constLine = line;
    //printf("%s\n", constLine);
//    return constLine;
}

// =============================================== SPOJ RETAZEC ======================================
/**< Pomocna funkcia, ktora vie "spajat" reatzec pomocou kopirovania. Vdaka tejto funkcii mozem ukladat jednotlive saves pod roznymi nazvami */

char* spoj_retazec(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

// =============================================== FUNKCIA SET MENO ======================================

/**< funkcia vr�ti smernik ako znaky, ktore sa pouzivaju na nazov suboru */

char *set_meno()
{
    printf("Zadajte ulozeneho subora: ");
    char *ptr = 0;
    int size = 0;
    char c;

    do
    {
        c = getchar();
        size++;
        ptr = (char*) realloc(ptr, size);
        ptr[size-1] = c;
    }
    while (c != '\n' && c != EOF);

    ptr[size-1] = '\0';

    return ptr;


}

// =============================================== FUNKCIA SAVE ======================================

/**< Hlavna funkcia na ukladanie dat po stlaceni tlacidla ulozenia */

void save(Isle * p_isle[], int pocet_rokov, int pocet_ostrovov, int pocet_hracov, char * save_name)
{
    //char *save_name;
    char *saves = "saves/"; //nazov zlozky kde su jednotlive saves
    //save_name = set_meno();
    
    char *full_name_bez_csv; //v premennej je nazov zlosky bez suboru
    char *full_name; //v premennej je nazov zlosky so suborom
    
    full_name_bez_csv = spoj_retazec(saves, save_name); //spoji saves s menom ulozeneho suboru
    mkdir(full_name_bez_csv); //vytvori zlozku saves

    //Vytvorenie cesty pre subor
    full_name = spoj_retazec(full_name_bez_csv, "/pocet_rokov.csv");
    FILE* subor_pr = fopen(full_name, "w");
    fprintf(subor_pr, "%d", pocet_rokov);
    fclose(subor_pr);

    //Vytvorenie cesty pre subor
    full_name = spoj_retazec(full_name_bez_csv, "/pocet_ostrovov.csv");
    FILE* subor_po = fopen(full_name, "w");
    fprintf(subor_po, "%d", pocet_ostrovov);
    fclose(subor_po);

    //Vytvorenie cesty pre subor
    full_name = spoj_retazec(full_name_bez_csv, "/pocet_hracov.csv");
    FILE* subor_pm = fopen(full_name, "w");
    fprintf(subor_pm, "%d", pocet_hracov);
    fclose(subor_pm);

    //Vytvorenie cesty pre subor, kde su ulozene polia ostrovov
    full_name = spoj_retazec(full_name_bez_csv, "/save.csv");
    FILE* subor = fopen(full_name, "w");

    int i;
    if (subor == NULL)
    {
        printf("Subor sa nepodarilo nacitat!\n");
        exit(1);
    }

    //fprintf(subor, "%d;", pocet_rokov);
    //fprintf(subor, "%d\n", pocet_ostrovov);

    const char * oddelovac = ";"; // oddelovaci prvok poloziek je ciarka
    for(i=0; i<pocet_ostrovov; i++)
    {
        fprintf(subor, "%s", p_isle[i]->name);
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->forests  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->villages  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->farms  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->pop  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->rats  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->food  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->wood  );
        fprintf(subor, "%s",oddelovac);
        fprintf(subor, "%i", p_isle[i]->moai  );
        fprintf(subor, "\n");
    }

    fclose(subor);
}

// =============================================== ULIZT VOLANIE FUNKCIE SAVE() ======================================

/**< Funkcia riesi podmioenku na z�klade podfunkcie porovnaj_znak(), ktora vracala 0, 1, 2.  */
void chcete_ulozit( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov, int pocet_hracov)
{
    int znak;
    znak = porovnaj_znak();

    if(znak == 1)
    {
        char save_name[100];
        printf("Zadajte nazov, pod ktorym chcete hru ulozit: ");
        scanf("%s", &save_name);
        save(ostrovy, pocet_rokov, pocet_ostrovov, pocet_hracov, save_name);
        printf("Hra sa ulozila");
    }
    else if(znak == 2)
    {
        char *save_name = "default";
        save(ostrovy, pocet_rokov, pocet_ostrovov, pocet_hracov, save_name);
        printf("Hra sa ulozila");
    }
    else
    {
        printf("Pokracujete bez ulozenia");
    }
}

// =============================================== SET POCET HRACOV ======================================
/**< Funkcia vrati celociselnu hodnotu kolko hracov bude hrat hru ostrovy */
int set_pocet_hracov(int pocet_ostrovov)
{
    int hracov;
    printf ("Zadajte pocet hracov, ktory budu ovladat ostrov");
    scanf("%d", &hracov);

    if (hracov > pocet_ostrovov)
    {
        hracov = 1;
    }
    else
    {
        hracov;
    }
    return hracov;
}

// =============================================== NAINICIALIZUJ OSTROVY ======================================
/**
 *
 * Hlavna funkcia, ktora vytvara dynamick� pole ostrovov
 * volanie struktury
 * v dalsej funkcii sa spravi inicializacia ostrova pre n ostrovov
 *
 */

Isle*  create_island()
{
    struct _isle_t* p_isle =(struct _isle_t*) malloc(sizeof(struct _isle_t));

    printf("Zadajte meno ostrova: ");
    scanf("%s",p_isle->name);
    p_isle->forests  = get_uniform(1, 55);
    p_isle->villages = get_uniform(1, 3);
    p_isle->farms = get_uniform(1, 15);
    p_isle->pop  = get_uniform(1, 90);
    p_isle->rats = get_uniform(1, 3);
    p_isle->food = get_uniform(1, 7);
    p_isle->wood = get_uniform(1, 7);
    p_isle->moai = 0;
    p_isle->pSettings = &GlobalSettings;

    return p_isle;
}

// =============================================== NASTAV OSTROVY ======================================
/**
 *
 * Hlavna funkcia, ktora vytvara dynamick� pole ostrovov
 * volanie struktury
 * Ak je load, tak v tejto funkcii sa nacitaju ulozene ostrovy a nainicialuzuju sa
 *
 */
Isle * create_island_from_file(FILE * subor)
{
    struct _isle_t* p_isle =(struct _isle_t*) malloc(sizeof(struct _isle_t));
    char *temp;

//   printf("%s", readItem(subor));

    //p_isle->moai = atoi(readItem(subor));

    temp = readItem(subor);
    strcpy(p_isle->name, temp );
    free(temp);

    temp = readItem(subor);
    p_isle->forests  = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->villages = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->farms = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->pop  = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->rats = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->food = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->wood = atoi(temp);
    free(temp);

    temp = readItem(subor);
    p_isle->moai = atoi(temp);
    free(temp);

    p_isle->pSettings = &GlobalSettings;

    return p_isle;

}
// =============================================== NASTAV OSTROVY ZO SUBORU ======================================
/**
 *
 * Hlavna funkcia, ktora vytvara dynamick� pole ostrovov
 * volanie struktury
 * Funkcia nastavi ostrovy podla poctu zvolen�ch ostrovov
 *
 */
int nastav_ostrovy_from_file( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov, char cesta[100])
{
    int i;
    FILE * subor;

    if ( ( subor = fopen( cesta, "r" ) ) == NULL )  //Reading a file
    {
        printf( "Subor sa nepodarilo nacitat.\n" );
        return 0;
    }

    for(i=0; i<pocet_ostrovov; i++)
    {
        ostrovy[i] = create_island_from_file(subor);
    }
    fclose(subor);
}


 // =============================================== EMIGRACIA ======================================
 /**
 *
 * Hlavna funkcia, ktora riesi emigraciu na ostrove a preroydelovanie emigrantov
 * volanie struktury
 * Vypocita sa pocet emigrantov
 * 20% emigrantov zahynie, zvysok sa deli v pomere k hodnosti ostrovu
 *
 */
int emigracia( Isle * p_isle[], int pocet_ostrovov)
{
    //zakladna inicializacia premennych
    int i;
    double atraktivnost_ostrova;
    double nastav_moai;
    double emigrants_s = 0;
    double neprezili;
    double emigrants;
    double volne_miesto;
    double zasoby_jedla;
    double koeficient_moai;
    double koeficient_rozdelenia;
    double ostrovy_splnajuce_podmienku;
    double pocet_moai;
    int emigrants_fin;

    // vypocita sucet vsetkych emigrantov na ostrovoch
    for(i=0; i<pocet_ostrovov ; i++)
    {
        emigrants = yearly_update(p_isle[i]);
        emigrants_s += emigrants;
    }
    emigrants_s = (emigrants_s * 80) / 100; //20% zahynulo pri emigracii

    // zacne delit vsetkych emigrantov
    for(i=0; i<pocet_ostrovov ; i++)
    {
        if(yearly_update(p_isle[i]) > 0)
        {
            volne_miesto = (p_isle[i]->villages * GS(PopPerVillage)) - p_isle[i]->pop;
            zasoby_jedla = (p_isle[i]->villages * GS(PopPerVillage)) - p_isle[i]->food;

            koeficient_moai =
                (p_isle[i]->forests + p_isle[i]->villages + p_isle[i]->farms +
                 p_isle[i]->pop + p_isle[i]->rats + p_isle[i]->food +
                 p_isle[i]->wood + p_isle[i]->moai);

            pocet_moai   =  p_isle[i]->moai * koeficient_moai; //moai ma 100x vyssiu prioritu, ako ostatne, pretoze na moai treba 500 drevo a 500pop. Co je zhruba 1/10/2 narocnosti

            atraktivnost_ostrova = volne_miesto + zasoby_jedla + pocet_moai;

            //koeficient_rozdelenia = atraktivnost_ostrova/ostrovy_splnajuce_podmienku;
        }
    }
    koeficient_rozdelenia = emigrants_s / atraktivnost_ostrova;

    //zacina sa delenie emigrantov do ostrovov
    for(i=0; i<pocet_ostrovov ; i++)
    {
        if (p_isle[i]->pop < p_isle[i]->villages * GS(PopPerVillage))
        {
            volne_miesto = (p_isle[i]->villages * GS(PopPerVillage)) - p_isle[i]->pop;
            zasoby_jedla = (p_isle[i]->villages * GS(PopPerVillage)) - p_isle[i]->food;

            koeficient_moai =
                (p_isle[i]->forests + p_isle[i]->villages + p_isle[i]->farms +
                 p_isle[i]->pop + p_isle[i]->rats + p_isle[i]->food +
                 p_isle[i]->wood + p_isle[i]->moai);

            pocet_moai   =  p_isle[i]->moai * koeficient_moai;

            emigrants_fin = koeficient_rozdelenia * atraktivnost_ostrova;
            return  emigrants_fin;
        }
    }
} //end of function



// =============================================== ZOBRAZ OSTROVY ======================================
/**
 *
 * Hlavna funkcia zobrazujuca ostrovy
 * vo funkcii su pouzite jednotliv� funkcie, ktore boli vyssie zadefinovane
 *
 *
 */
 
void zobraz_ostrov( Isle *  p_isle[], int pocet_rokov, int pocet_ostrovov, int pocet_hracov)
{
    int year, action, error;
    int emigrants;
    int emigrants_s = 0;
    int i;
    int atraktivnost;
    int prerozdelenie_emigrantov = 0;


    //printf("Starting out...\n ");
    ciara();
    printf("ZACINAME HRAT....\n");
    ciara();

    for (year = 1; year <= pocet_rokov; year++) //for pre pocet rokov hry
    {
        printf("ROK: %d\n\n", year);
        for(i=0; i<pocet_ostrovov ; i++) //for pre pocet ostrovov hry
        {

            if (i < pocet_hracov) //tu sa triesi multyplayer, prv�ch n pol� patr�  hr�com
            {
                printf("TERAZ HRA HRAC %d. STLACENIM PRISLUSNEJ KLAVESY VYBERIETE NASLEDOVNU VOLBU:\n\n", i+1);
                action = set_action();  //algoritmus pre pouzivatela riesi GUI-funkcia  s moznostou volby
                //ciara();
            }
            else // druh�ch n poli pocitacu
            {
                //action = get_uniform(0, NUM_ACTIONS-1);
                action = get_uniform_ui(ostrovy, pocet_ostrovov); // algoritmus pre PC riesi moja umela inteligencia
            }

            printf("Villagers decided to %s, and it was ", VillageActionNames[action]);
            error = VillageActions[action](p_isle[i]);
            printf("%s.\n", error == ERR_OK ? "done" : "impossible");

            emigrants = yearly_update(p_isle[i]) + prerozdelenie_emigrantov; //vysledny pocet emigrantov aj s prerozdelenim
            debug_print_isle(stdout, p_isle[i]);

            printf("%i people emigrated.\n", emigrants); //kolko emigrantov emigrovalo
            printf("\n");
            emigrants_s += emigrants; //pocet emigrantov

        } // end ostrovy
        ciara();

        //vypis celkoveho poctu emigrantov
        printf("NA CELKOVOM POCTE OSTROVOV %d BOL CELKOVY POCET EMIGRANTOV %d \n", i, emigrants_s);

        //rozdelenie emigrantov podla pomeru, ktory riesi funkcia amigracia( ostrovy, pocet_ostrovov);
        prerozdelenie_emigrantov = emigracia( ostrovy, pocet_ostrovov);
        printf("NA CELKOVOM POCTE OSTROVOV %d BOLO PREROZDELENZCH %d EMIGRANTOV\n", i, prerozdelenie_emigrantov);

        //zobrazi maximalne stavy surov�n na ostrove pre dan� n�zov ostrova
        max_na_ostrove(ostrovy, pocet_ostrovov);
        chcete_ulozit(p_isle, pocet_rokov, pocet_ostrovov, pocet_hracov);
        ciara();
    } // end roky
}


// =============================================== NASTAV OSTROVY ======================================

//funkcia nainicializuje ostrovy z funkcie create_island(); vo fore pre n ostrovov
void nastav_ostrovy( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov)
{
    int i;
    for(i=0; i<pocet_ostrovov; i++)
    {
        ostrovy[i] = create_island();
    }
}

// =============================================== ZRUS OSTROVY ======================================

//funkcia zrus� ostrovy a vycisti pamat
void zrus_ostrovy( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov)
{
    int i;
    for( i=0; i<pocet_ostrovov; i++)
    {
        free( ostrovy[i] );
    }
}

// =============================================== TOTO JE HLAVNA FUNKCIA: SPUSTI ======================================

//hlavna funkcia na spustanie hry

void spusti()
{
    init_random_seed();
    int mkdir_return_value = mkdir("saves", 0660);
    int pocet_rokov;
    int pocet_ostrovov;
    int pocet_hracov;

    intro();

    int porovnaj_znak = porovnaj_znak_load(); //spyta sa uivatela, cic che nacitat htu so suboru, alebo chce nacitat default, alebo novu hru,

    // AK DEFAULT
    if (porovnaj_znak == 2)
    {
        pocet_hracov = nacitaj_integer("saves/default/pocet_hracov.csv"); //nacita zo suboru pocet hracov ktory bol po poslednom ulozeni
        pocet_rokov = nacitaj_integer("saves/default/pocet_rokov.csv"); // nacita pocet rokov tej hry, ktor� sa hrala
        pocet_ostrovov = nacitaj_integer("saves/default/pocet_ostrovov.csv"); //nacita posledne nastavenia ostrova
        nastav_ostrovy_from_file(ostrovy, pocet_rokov, pocet_ostrovov, "saves/default/save.csv");
        zobraz_ostrov(ostrovy, pocet_rokov, pocet_ostrovov, pocet_hracov);
    }

    //AK NACITAT Z VLASTNEHO SUBORU? KTORY BOL PREDTYM ULOZENY
    else if (porovnaj_znak == 1)
    {
        char *full_name_1;
        char *full_name_2;
        char *full_name_3;
        char *full_name_4;

        char save_name[100];
        char *saves = "saves/";
        char *full_name_bez_csv;

        printf("Zadajte nazov, suboru, ktory chcete nacitat: ");
        scanf("%s", &save_name);

        full_name_bez_csv = spoj_retazec(saves, save_name);

        full_name_1 = spoj_retazec(full_name_bez_csv, "/pocet_hracov.csv");
        full_name_2 = spoj_retazec(full_name_bez_csv, "/pocet_rokov.csv");
        full_name_3 = spoj_retazec(full_name_bez_csv, "/pocet_ostrovov.csv");
        full_name_4 = spoj_retazec(full_name_bez_csv, "/save.csv");

        pocet_hracov = nacitaj_integer(full_name_1); //nacita zo suboru pocet hracov ktory bol po poslednom ulozeni
        pocet_rokov = nacitaj_integer(full_name_2); // nacita pocet rokov tej hry, ktor� sa hrala
        pocet_ostrovov = nacitaj_integer(full_name_3); //nacita posledne nastavenia ostrova
        nastav_ostrovy_from_file(ostrovy, pocet_rokov, pocet_ostrovov, full_name_4);
        zobraz_ostrov(ostrovy, pocet_rokov, pocet_ostrovov, pocet_hracov);

    }
    //AK ZACAT HRAT NOVU HRU
    else
    {
        pocet_rokov = set_pocet_rokov();
        pocet_ostrovov = set_pocet_ostrovov();
        pocet_hracov = set_pocet_hracov(pocet_ostrovov);
        nastav_ostrovy(ostrovy, pocet_rokov, pocet_ostrovov);
        zobraz_ostrov(ostrovy, pocet_rokov, pocet_ostrovov, pocet_hracov);
    }

    //zrusenie ostrovov
    zrus_ostrovy(ostrovy, pocet_rokov, pocet_ostrovov);
}
