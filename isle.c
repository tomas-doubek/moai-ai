#include <stdio.h>
#include "random.h"
#include "isle.h"

Settings GlobalSettings = {
   10, //FoodPerFieldMin
   20, //FoodPerFieldMax
 
   -10, //BadYearOffset
   20,  //BadYearChance
   
   1, //WoodLossPercent
   5, //FoodLossPercent
   
   10, //PopPerFarm
   50, //WoodPerFarm
   
   100, //PopPerVillage
   200, //MaxPopPerVillage
   100, //WoodPerVillage
   5,   //Moai multiplier

   20,  //BuildAccidentChance
   5,   //ChopForestAccidentChance
   15,  //FishingAccidentChance
   
   25,  //FoodGatherChance
   50,  //WoodGatherChance  
   75,  //FishingChance
   
   10,  //WoodPerBoat
   50,  //PopPerBoat
   
   4,   //GrowthRate
   2,   //EmigrationRate
   
   75,  //Starvation Chance
};


/**
 * Pomocny vypis, usporny a nie prilis prehladny
 */
int debug_print_isle(FILE* f, const Isle *pIsle)
{
    int total = 0;
    total += fprintf(f, "Isle of %s\n", pIsle->name);
    total += fprintf(f, "   FOR: %5i%%  FARM: %3i  VIL: %3i MOAI: %5i\n", 
                     pIsle->forests, pIsle->farms, pIsle->villages, pIsle->moai);
    total += fprintf(f, "   POP: %6i  ",    
                     pIsle->pop);
    total += fprintf(f, "   FOOD: %5i  WOOD: %5i\n", 
                         pIsle->food, pIsle->wood);
    return total;
}

/**
 * Vrati pocet volnych miest na ostrove
 */
int get_empty(const Isle *pIsle)
{
    return 100-(pIsle->forests + pIsle->farms + pIsle->villages + pIsle->moai);
}

/**
 * Village actions
 */
 
VillageAction VillageActions[] = {
va_build_farm,  va_build_village, va_chop_forest, va_gather_wood, 
va_gather_food, va_go_fishing,    va_build_moai,  va_do_nothing,
};
 
const char* VillageActionNames[] = {
"Build Farm", "Build Village", "Chop Forest", "Gather Wood", 
"Gather Food", "Go Fishing",   "Build Moai",  "Do Nothing",
}; 
 
//PRE: POP >= PopPerVillage+PopPerFarm, WOOD >= WoodPerFarm, 1 Empty space
//POST: FARM++, POP-=PopPerFarm, WOOD-=WoodPerFarm
//uses global settings, except wood per farm
int va_build_farm(Isle* pIsle) 
{
    if (get_empty(pIsle) < 1)
        return ERR_NO_ROOM;

    if (pIsle->pop < GS(PopPerVillage)+GS(PopPerFarm))
        return ERR_NO_POP;
    
    if (pIsle->wood < LS(WoodPerFarm))
        return ERR_NO_WOOD;
    
    pIsle->farms++;    
    pIsle->pop  -= GS(PopPerFarm);    
    pIsle->wood -= LS(WoodPerFarm);    
        
    return ERR_OK;    
}

//PRE: POP > PopPerVillage*(VIL+1/2), WOOD >= WoodPerVillage
//POST: VIL++, WOOD-=WoodPerVillage, ACCIDENTs
//uses global settings, except build accident chance
int va_build_village(Isle* pIsle) 
{
    if (get_empty(pIsle) < 1)
        return ERR_NO_ROOM;

    if (pIsle->pop < GS(PopPerVillage)*pIsle->villages + GS(PopPerVillage)/2)
        return ERR_NO_POP;
    
    if (pIsle->wood < GS(WoodPerVillage))
        return ERR_NO_WOOD;
    
    pIsle->villages++;    
    pIsle->wood -= GS(WoodPerVillage);    

    //some of builders can die
    pIsle->pop  -= get_binomial(GS(PopPerVillage)/2, LS(BuildAccidentChance));    
        
    return ERR_OK;    
}

//PRE: POP >= 100
//POST: FOR-=VIL, WOOD=RND(VILxWoodPerVillage x[50-150]), ACCIDENTs
//uses global PopPerVillage but local WoodPerVillage and acccident chance
int va_chop_forest(Isle* pIsle)
{
    int num_forests;
    if (pIsle->pop < GS(PopPerVillage))
        return ERR_NO_POP;
    
    //number of chopped forests: cannot exceed number of forests/villages
    //   and max 1 per PopPerVillage population
    num_forests = MIN(pIsle->pop/GS(PopPerVillage), MIN(pIsle->villages, pIsle->forests));
    
    pIsle->forests -= num_forests;    
    pIsle->wood += get_sum_uniform(num_forests, LS(WoodPerVillage)/2, 3*LS(WoodPerVillage)/2);    
    
    //accidents
    pIsle->pop  -= get_binomial(num_forests, LS(ChopForestAccidentChance));    
    
    return ERR_OK;
}

//PRE: FOR > 0
// get some wood from forests, local settings
int va_gather_wood(Isle* pIsle)
{
    int num_gatherers;
    if (pIsle->forests < 1)
        return ERR_NO_FOREST;
    
    num_gatherers = MIN(pIsle->pop, pIsle->forests*LS(PopPerVillage));
    pIsle->wood += get_binomial(num_gatherers, LS(WoodGatherChance));
    
    return ERR_OK;
}

//PRE: FOR > 0
// get some food from forests, local settings
int va_gather_food(Isle* pIsle)
{
    int num_gatherers;
    if (pIsle->forests < 1)
        return ERR_NO_FOREST;
    
    num_gatherers = MIN(pIsle->pop, pIsle->forests*LS(PopPerVillage));
    pIsle->food += get_binomial(num_gatherers, LS(FoodGatherChance));
    
    return ERR_OK;
}

//PRE: WOOD > 0
// uses wood to build boats and catch some fishes, accidents possible
// local settings
int va_go_fishing(Isle* pIsle)
{
    int num_fishers, num_boats, accidents;
    if (pIsle->wood < 1)
        return ERR_NO_WOOD;
    
    //must have enough wood and pop for boats, 1 wood per WoodPerFisher
    num_boats = MIN(pIsle->pop/LS(PopPerBoat), pIsle->wood/LS(WoodPerBoat));
    
    accidents = get_binomial(num_boats, LS(FishingAccidentChance));
    
    num_boats  -= accidents;
    num_fishers = num_boats * LS(PopPerBoat);

    pIsle->wood -= accidents*LS(WoodPerBoat);
    pIsle->pop  -= accidents*LS(PopPerBoat);
     
    pIsle->food += get_binomial(num_fishers, LS(FishingChance));
    
    return ERR_OK;
}

//PRE: empty space, POP >= MoaiMultiplier*PopPerVillage, WOOD >= MoaiMultiplier*WoodPerVillage
// uses global settings instead of local, except build accident chance
int va_build_moai(Isle* pIsle)
{
    if (get_empty(pIsle) < 1)
        return ERR_NO_ROOM;

    if (pIsle->pop < GS(PopPerVillage)*GS(MoaiMultiplier))
        return ERR_NO_POP;
    
    if (pIsle->wood < GS(WoodPerVillage)*GS(MoaiMultiplier))
        return ERR_NO_WOOD;
    
    pIsle->moai++;    
    pIsle->wood -= GS(WoodPerVillage)*GS(MoaiMultiplier);    

    //some of builders can die
    pIsle->pop  -= get_binomial(pIsle->pop, LS(BuildAccidentChance));    
        
    return ERR_OK;        
}

int va_do_nothing(Isle* pIsle)
{
    return ERR_OK;
}

/**
 * Yearly update of wood store, returns total loss
 */
int update_wood_stores(Isle* pIsle)
{
    int lost = 0;
    if (pIsle->wood > 0 && LS(WoodLossPercent) > 0)
    {
        lost = get_binomial(pIsle->wood,LS(WoodLossPercent));
        pIsle->wood -= lost;
    }
    return lost;       
}

/**
 * Yearly update of food store, returns total loss
 */
int update_food_stores(Isle* pIsle)
{
    //number of rats grows, if food exceeds villagers
    //and lowers, if food store is less than village capacity
    //and die out, if there is no stored food
    int lost = 0;
    
    if (pIsle->food == 0)
    {
        pIsle->rats = 0;
        return 0;     
    }
    
    if (pIsle->food > pIsle->pop)
        pIsle->rats++;
    else if (pIsle->rats > 0 && pIsle->food < pIsle->villages*GS(PopPerVillage))
        pIsle->rats--; 
    
    if (pIsle->food > 0 && LS(FoodLossPercent)+pIsle->rats > 0)
    {
        lost = get_binomial(pIsle->food,LS(FoodLossPercent)+pIsle->rats);
        pIsle->food -= lost;
    }
    
    return lost;       
}
 
/**
 * Farm update: can bring some population, produces food
 */
int do_farming(Isle* pIsle)
{
    int farmers = pIsle->farms * GS(PopPerFarm);
    int harvest = 0;
    if (farmers <= 0)
        return 0;
    
    if (get_alternative(LS(BadYearChance)))
    {
        //bad year, no population growth, weak harvest
        harvest = get_sum_uniform(pIsle->farms , LS(FoodPerFieldMin)-LS(BadYearOffset), 
                               LS(FoodPerFieldMax)-LS(BadYearOffset));
        pIsle->food += harvest;
    }
    else
    {
        //good year, farm population growth will add to village population
        // good harvest
        pIsle->pop += get_binomial(farmers, LS(GrowthRate));
        harvest = get_sum_uniform(pIsle->farms, LS(FoodPerFieldMin), LS(FoodPerFieldMax));
        pIsle->food += harvest;
    }
    return harvest;
}
 
/**
 * Yearly update of population, returns emigrants
 */
int update_population(Isle* pIsle)
{
    int rate, emigrants = 0, hungry;
    
    //no population, no growth
    if (pIsle->pop == 0)
        return 0;
    
    //if there is enough food, population just grows
    // uses global settings for village capacity, local growth rate
    if (pIsle->food >= pIsle->pop)
    {
         //plenty of food means faster growth
         rate = LS(GrowthRate) + get_log_ratio(pIsle->food, pIsle->pop);   

         //if there is excess population, growth is lower due to sanitation problems
         if (pIsle->pop > pIsle->villages*GS(PopPerVillage))
         {
             rate -= get_log_ratio(pIsle->pop - pIsle->villages*GS(PopPerVillage), pIsle->villages);
         }
                
         pIsle->food -= pIsle->pop;        
         if (rate > 0)
             pIsle->pop += get_binomial(pIsle->pop, rate);
    }
    else //otherwise, population goes hungry... no growth, some die, some emigrate
    {
         hungry = pIsle->pop - pIsle->food;

         pIsle->food = 0;
         pIsle->pop -= hungry;

         emigrants = get_binomial(hungry, 100-LS(StarvationChance));
    }
    
    //finally, there can be emigrants from overcrowding
    if (pIsle->pop > pIsle->villages*GS(PopPerVillage))
    {
        rate = LS(EmigrationRate) 
                 + get_log_ratio(pIsle->pop - pIsle->villages*GS(PopPerVillage), 
                                 GS(PopPerVillage));
		if (rate > 0)
            emigrants = get_binomial(pIsle->pop - pIsle->villages*GS(PopPerVillage), rate);
        pIsle->pop -= emigrants;
    }
                
    return emigrants;
}
 
/**
 * Yearly update, returns number of emigrants
 */
int yearly_update(Isle* pIsle)
{
    int woodloss, foodloss, emigrants, harvest;     //losses for debugging
    
    woodloss = update_wood_stores(pIsle);   
    foodloss = update_food_stores(pIsle); 
    harvest  = do_farming(pIsle);   
    
    emigrants = update_population(pIsle);
    
    return emigrants;
}
  
