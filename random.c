#include <stdlib.h>
#include <time.h>

// inicializacia generatora na zaklade casu spustenia
//  Umoznuje ale SAVE/LOAD zmeny vysledku ;)
void init_random_seed()
{
    srand(time(0));
}

// vracia jednotku hodnotu s pravdepodobnostou p %, inak 0
int get_alternative(int p)
{
    return rand() % 100 < p ? 1 : 0;
}

// vracia kolkokrat sa z n pokusov splnila udalost s pravdepodobnostou p %
//  pozn.: pomerne neefektivny algoritmus, obzvlast pre velke n
int get_binomial(int n, int p)
{
    int i, sum = 0;
    for (i = 0; i < n; i++)
        sum += get_alternative(p);
    return sum;
}

// vracia rovnomerne nahodne cele cislo z intervalu [a,b] (vratane hranic)
// PRE: a <= b
int get_uniform(int a, int b)
{
    return rand() % (b-a+1) + a;
}

// vracia sucet nahodnych rovnomerne rozdelenych cisel
//  pozn.: pomerne neefektivny algoritmus, obzvlast pre velke n
int get_sum_uniform(int n, int a, int b)
{
    int i, sum = 0;
    for (i = 0; i < n; i++)
        sum += get_uniform(a, b);
    return sum;
}

//pomocna funkcia, zisti logaritmicky pomer dvoch premennych (base2)
int get_log_ratio(int citatel, int menovatel)
{
    int ratio, log = 0;
    ratio = (citatel > menovatel) ? citatel/menovatel : menovatel/citatel;

    while (ratio > 0)
    {
        log++;
        ratio >>= 1; //bitovy posun je to iste co delenie 2
    }

    return (citatel > menovatel) ? log : -log;
}
