/**************************************************************************
 * Zadanie z AaP: Simulacia ostrovnej ekonomiky
 * Kostra zadania: (C) Pavol Zajac, 2014
 *
 * Riesenie vypracoval:
 *    [PRIEZVISKO MENO, ID, KRUZOK]
 *
 * Ulohy (napiste [X] pred komplet splnenu ulohu, [-] pred ciastocne riesenie):
 *  [X] 1. Inicializacia pola n ostrovov, n voli pouzivatel (dynamicke pole)
 *  [X] 2. Serializacia stavu (nacitanie/zapis do suboru)
 *  [X] 3. Implementacia migracie a/alebo skusenosti
 *  [X] 4. User interface
 *  [X] 5. Umela inteligencia
 *
 * Extra body:
 *    [NAPISTE, CO EXTRA STE IMPLEMENTOVALI]
 *  [X] BONUS 1. Multyplayer
 *  [X] BONUS 2. UKLADANIE DAT DO CSV FORMATU
 *
 * Ak to nie je nutne, EXISTUJUCE STRUKTURY A FUNKCIE NEMENIT!
 *  Novy kod dajte do vlastneho zdrojoveho/hlavickoveho suboru
 *  Pripadne zmeny v povodnych suboroch oznacte komentarmi s vasimi inicialkami
 *  Piste prehladny a citatelny kod, pomenuvajte premenne celymi nazvami
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "random.h"
#include "isle.h"
#include "doubek.tomas.h"

int main()
{    
    spusti();
    system("pause");
    return 0;
}
