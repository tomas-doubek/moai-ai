/*************************************************************************
 * Pomocne funkcie pre generovanie nahodnych cisel.
 * (C) 2014, Pavol Zajac
 *************************************************************************/

#ifndef __RANDOM_H 
#define __RANDOM_H 
 
// inicializacia generatora na zaklade casu spustenia 
//  Umoznuje ale SAVE/LOAD zmeny vysledku ;)
void init_random_seed();

// vracia jednotku hodnotu s pravdepodobnostou p %, inak 0
int get_alternative(int p);

// vracia kolkokrat sa z n pokusov splnila udalost s pravdepodobnostou p %
int get_binomial(int n, int p);

// vracia rovnomerne nahodne cele cislo z intervalu [a,b] (vratane hranic)
int get_uniform(int a, int b);

// vracia sucet n rovnomerne nahodnych celych cisel z intervalu [a,b] (vratane hranic)
int get_sum_uniform(int n, int a, int b);

//pomocna funkcia, zisti logaritmicky pomer dvoch premennych
int get_log_ratio(int citatel, int menovatel);
#endif //__RANDOM_H 
