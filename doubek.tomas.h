#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "random.h"
#include "isle.h"

int set_pocet_rokov();
int set_pocet_ostrovov();
void ciara();
void intro();
int porovnaj_znak();
int porovnaj_znak_load();
int set_action();
int get_uniform_ui(Isle * p_isle[], int pocet_ostrovov);
void max_na_ostrove(Isle * p_isle[], int pocet_ostrovov);
int nacitaj_integer(const char* subor);
char *readItem(FILE *file);
char* spoj_retazec(char *s1, char *s2);
char *set_meno();
void save(Isle * p_isle[], int pocet_rokov, int pocet_ostrovov, int pocet_hracov, char * save_name);
void chcete_ulozit( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov, int pocet_hracov);
int set_pocet_hracov(int pocet_ostrovov);
Isle*  create_island();
Isle * create_island_from_file(FILE * subor);
int nastav_ostrovy_from_file( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov, char cesta[100]);
int emigracia( Isle * p_isle[], int pocet_ostrovov);
void zobraz_ostrov( Isle *  p_isle[], int pocet_rokov, int pocet_ostrovov, int pocet_hracov);
void nastav_ostrovy( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov);
void zrus_ostrovy( Isle * ostrovy[], int pocet_rokov, int pocet_ostrovov);
void spusti();
